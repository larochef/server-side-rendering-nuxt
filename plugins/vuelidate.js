import Vue from 'vue';
import Vuelidate from 'vuelidate';

// Attention a cette erreur avec vuelidate : https://github.com/declandewet/vue-meta/issues/187
Vue.use(Vuelidate);
