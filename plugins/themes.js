import Vue from 'vue';

export default ({ app }) => {
    const theme = app.context.env.style;
    switch (theme) {
        case 'pharmaservices':
            Vue.material.theming.theme = 'pharmaservices';
            break;
        case 'clicbeaute':
            Vue.material.theming.theme = 'clicbeaute';
            break;
        case 'clicanimal':
            Vue.material.theming.theme = 'clicanimal';
            break;
        default:
            Vue.material.theming.theme = 'clicsante';
    }
};
