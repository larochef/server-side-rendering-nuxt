module.exports = {
    links: {
        home: 'Accueil',
        about: 'À propos',
        login: 'Connexion'
    },
    home: {
        title: 'Bienvenue',
        introduction: "Ceci est un texte d'introduction en Français."
    },
    about: {
        title: 'À propos',
        introduction: "Cette page est faite pour vous donner plus d'informations."
    },
    login: {
        button: 'Se connecter',
        forgotpassword: 'Mot de passe oublié?',
        invalidEmail: 'Le format du courriel est invalide',
        noEmail: 'Vous devez entrer votre courriel',
        noPassword: 'Vous devez entrer un mot de passe',
        welcome: 'Veuillez vous authentifier pour accéder au planificateur de rendez-vous.'
    },
    base: {
        action: 'Action',
        add: 'Ajouter',
        address: 'Adresse',
        cancel: 'Annuler',
        category: 'Catégorie',
        close: 'Fermer',
        del: 'Supprimer',
        edit: 'Modifier',
        email: 'Courriel',
        name: 'Nom',
        password: 'Mot de passe',
        save: 'Enregistrer',
        state: 'État'
    },
    error: {
        auth: {
            globalTitle: 'Erreur d\'authentification',
            globalMessage: 'Une erreur c\'est produite, veuillez réessayer.',
            networkTitle: 'Erreur réseau...',
            networkMessage: 'La requête n\'a pas pu être compléter',
            title401: 'Erreur d\'authentification...',
            message401: 'Assurez-vous de bien entrer votre courriel et votre mot de passe...',
            title404: 'Un problème est survenu...',
            message404: 'La requête n\'a pas pu être complété...'
        }
    }
};
