module.exports = {
    links: {
        home: 'Home',
        about: 'About',
        login: 'Login'
    },
    home: {
        title: 'Welcome',
        introduction: 'This is an introduction in English.'
    },
    about: {
        title: 'About',
        introduction: 'This page is made to give you more informations.'
    },
    login: {
        button: 'Login',
        forgotpassword: 'Forgot password?',
        invalidEmail: 'Invalid email format',
        noEmail: 'Please enter your email',
        noPassword: 'Please enter a password',
        welcome: 'Please authenticate to access the appointments manager.'
    },
    base: {
        action: 'Apply',
        add: 'Add',
        address: 'Address',
        cancel: 'Cancel',
        category: 'Category',
        close: 'Close',
        del: 'Delete',
        edit: 'Edit',
        email: 'Email',
        name: 'Name',
        password: 'Password',
        save: 'Save',
        state: 'State'
    },
    error: {
        auth: {
            globalTitle: 'Authentification Error',
            globalMessage: 'An error has occurred, please try again.',
            networkTitle: 'Network Error...',
            networkMessage: 'The request could not be completed',
            title401: 'Authentification Error...',
            message401: 'Make sure you enter correctly your email and password...',
            title404: 'A problem has occurred...',
            message404: 'The request could not be completed...'
        }
    }
};
