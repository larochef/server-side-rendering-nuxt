const pkg = require('./package');
const env = require('dotenv').config().parsed;

module.exports = {
    mode: 'universal',
    env: {
        api_endpoint: env.api_endpoint
    },

    /*
    ** Headers of the page
    */
    head: {
        title: pkg.name,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    /*
    ** Customize the progress-bar color
    */
    loading: {
        color: env.primary,
        height: '5px'
    },

    /*
    ** Global CSS
    */
    css: [
        '@/assets/scss/app.scss'
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '~/plugins/themes.js',
        { src: '~/plugins/vuelidate', ssr: false }
    ],

    /*
    ** Nuxt.js modules
    */
    modules: [
        '@nuxtjs/dotenv',
        'nuxt-vue-material',
        // ['@nuxtjs/google-tag-manager', { id: 'GTM-XXXXXXX' }],
        // ['@nuxtjs/google-analytics', { id: 'UA-12301-2' }],
        ['nuxt-i18n', {
            redirectRootToLocale: 'fr',
            defaultLocale: 'fr',
            noPrefixDefaultLocale: false,
            loadLanguagesAsync: true,
            langDir: 'locale/',
            locales: [
                {
                    code: 'fr',
                    iso: 'fr-CA',
                    name: 'Français',
                    langFile: 'fr.js'
                },
                {
                    code: 'en',
                    iso: 'en-CA',
                    name: 'English',
                    langFile: 'en.js'
                }],
            routes: {
                about: {
                    fr: '/a-propos',
                    en: '/about-us'
                }
            }
        }]
    ],

    /*
    ** Build configuration
    */
    build: {
        babel: {
            presets: [
                [ 'env', {
                    'modules': false,
                    'targets': { 'browsers': ['> 1%', 'last 2 versions', 'not ie <= 8'] }
                }],
                'es2015',
                'stage-2'
            ],
            plugins: ['transform-runtime'],
            env: {
                'test': {
                    'presets': ['env', 'stage-2'],
                    'plugins': ['istanbul']
                }
            }
        },
        extend(config, ctx) {
            // Run ESLint on save
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                });
            }
        }
    }
};
