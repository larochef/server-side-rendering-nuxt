// https://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module'
    },
    env: {
        mocha: true
    },
    globals: {
        browser: true,
        expect: true,
        sinon: true,
        _: true,
        $: true,
    },
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    extends: ['standard'],
    // required to lint *.vue files
    plugins: [
        'html',
        'mocha',
        'lodash'
    ],
    // add your custom rules here
    'rules': {
        "no-undef": 0,
        "lodash/import-scope": 0,
        "lodash/lodash-prefer-method": [0, {"ignoreMethods": ["includes","forEach"]}],
        "no-new": 0,
        "semi": ["error", "always"],
        "keyword-spacing": ["off", { "before": false, "after": false }],
        "brace-style": ["error", "stroustrup", { "allowSingleLine": true }],
        "padded-blocks": ["error", "never"],
        "space-before-function-paren": ["error", "never"],
        "indent": ["error", 4, { "SwitchCase": 1 }],
        // allow paren-less arrow functions
        'arrow-parens': 0,
        // allow async-await
        'generator-star-spacing': 0,
        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    }
};
