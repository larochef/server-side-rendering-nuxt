import test from 'ava';
import { Nuxt, Builder } from 'nuxt';
import { resolve } from 'path';

// We keep a reference to Nuxt so we can close
// the server at the end of the test
let nuxt = null;
const rootDir = resolve(__dirname, '..');

// Init Nuxt.js and start listening on localhost:4000
test.before('Init Nuxt.js', async t => {
    let config = {};
    try { config = require(resolve(rootDir, 'nuxt.config.js')); }
    catch (e) {}
    config.rootDir = rootDir; // project folder
    config.dev = false; // production build
    nuxt = new Nuxt(config);
    await new Builder(nuxt).build();
    nuxt.listen(4000, 'localhost');
});

// Example of testing only generated html
test('Route /fr/a-propos exits and render HTML', async t => {
    let context = {};
    const { html } = await nuxt.renderRoute('/fr/a-propos/', context);
    t.true(html.includes('<h1 class="red">Hello World!</h1>'));
});

// Example of testing via DOM checking
test('Route /fr/a-propos exits and render HTML with CSS applied', async t => {
    const window = await nuxt.renderAndGetWindow('http://localhost:4000/fr/a-propos/');
    const element = window.document.querySelector('.red');
    t.not(element, null);
    t.is(element.textContent, 'Hello World!');
    t.is(element.className, 'red');
    console.log(window.getComputedStyle(element));
    t.is(window.getComputedStyle(element).color, 'red');
});

// Close server and ask nuxt to stop listening to file changes
test.after.always('Closing server and nuxt.js', async t => {
    await nuxt.close();
});
