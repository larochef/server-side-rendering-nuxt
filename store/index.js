import Vuex from 'vuex';

const createStore = () => {
    return new Vuex.Store({
        state: {
            module: process.env.module,
            style: process.env.style
        },
        getters: {
            getModule: state => {
                return state.module;
            },
            getStyle: state => {
                return state.style;
            }
        },
        mutations: {},
        actions: {},
        modules: {
            // auth,
        },
        strict: true
    });
};

export default createStore;
